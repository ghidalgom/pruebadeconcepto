<?php

namespace Tests\Feature;

use App\Models\DoctrineUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use \Doctrine\ORM\EntityManagerInterface;

class DoctrineTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function pruebaDoctrine()
    {
        $doctrineUser = new DoctrineUser();
        $doctrineUser->setName('test3');
        $doctrineUser->setEmail('prueba3@test.com');
        $doctrineUser->setPassword('123456789');
        $entityManager = app(EntityManagerInterface::class);
        $entityManager->persist($doctrineUser);
        $entityManager->flush();
        $user = $entityManager->find(DoctrineUser::class, 1);
        $this->assertEquals('test3', $user->getName());
    }
}
