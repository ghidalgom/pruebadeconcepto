<?php

namespace App\Repository;

use App\Models\DoctrineUser;
use Doctrine\ORM\EntityManagerInterface;

class UserRepo
{
    public function create()
    {
        $doctrineUser = new DoctrineUser();
        $doctrineUser->setName('test2');
        $doctrineUser->setEmail('prueba2@test.com');
        $doctrineUser->setPassword('1234567');
        $entityManager = app(EntityManagerInterface::class);
        $entityManager->persist($doctrineUser);
        $entityManager->flush();
    }
}
